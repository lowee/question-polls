import React, { FC } from "react";
import { Page, Text } from "@zeit-ui/react";
import Head from "next/head";

interface LayoutProps {
  pageTitle?: string;
}

const Layout: FC<LayoutProps> = ({ pageTitle, children }) => {
  return (
    <div>
      <Head>
        <title>{pageTitle || "Question Polls"}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Page size="small">
        <Page.Header
          style={{
            padding: "12px",
          }}
          center
        >
          <h2>Welcome to Question Polls</h2>
        </Page.Header>
        <Page.Content>{children}</Page.Content>
        <Page.Footer>
          <Text>This is a test application made by Lorenz Weiß 2020</Text>
        </Page.Footer>
      </Page>
    </div>
  );
};

export default Layout;
