import React, { FC } from "react";
import { QuestionsData } from "../../types/question.types";
import Link from "next/link";
import { Grid, Card, Text } from "@zeit-ui/react";
import { FilePlus } from "@zeit-ui/react-icons";
import styles from "./QuestionList.module.scss";
import QuestionCard from "../questionCard/QuestionCard";

interface QuestionListProps {
  questions: QuestionsData;
}

const QuestionList: FC<QuestionListProps> = ({ questions }) => {
  return (
    <div>
      <Grid.Container gap={2} justify="center">
        <Grid xs={24} md={8}>
          <Link href="/questions/add" as={`/questions/add`}>
            <a>
              <Card className={styles.card} shadow type="success">
                <div className={styles.container}>
                  <FilePlus size={42} />
                  <Text> Add new Question</Text>
                </div>
              </Card>
            </a>
          </Link>
        </Grid>
        {Object.entries(questions).map(([id, question]) => (
          <Grid key={id} xs={24} md={8}>
            <Link href="/questions/[questionId]" as={`/questions/${id}`}>
              <a>
                <QuestionCard question={question} />
              </a>
            </Link>
          </Grid>
        ))}
      </Grid.Container>
    </div>
  );
};

export default QuestionList;
