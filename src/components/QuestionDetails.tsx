import React, { FC, useMemo, useState } from "react";
import { Table, Button, Grid, Spacer, Radio, useToasts } from "@zeit-ui/react";
import { Question } from "../types/question.types";
import VoteStatus from "./VoteStatus";
import { SaveStates } from "../pages/questions/[questionId]";

interface QuestionDetailsProps {
  question: Question;
  saveVotes: (choiceId: string) => Promise<void>;
  saveStatus: SaveStates;
}

const saveMessages = {
  success: "The vote was successfully saved",
  error: "Something went wrong, the vote couldn't be saved",
};

const QuestionDetails: FC<QuestionDetailsProps> = ({
  question,
  saveVotes,
  saveStatus,
}) => {
  const allVotes = question.choices.reduce((acc, el) => acc + el.votes, 0);
  const [, setToast] = useToasts();

  const [selectedVote, setSelectedVote] = useState(null);

  const onSave = async () => {
    const choiceId = selectedVote.split("choices/")[1];
    await saveVotes(choiceId);
    setSelectedVote(null);
    if (saveStatus !== "loading") {
      setToast({
        text: saveMessages[saveStatus],
        type: saveStatus,
      });
    }
  };

  const data = useMemo(
    () =>
      question.choices.reduce((acc, choice) => {
        let status = 0;
        if (allVotes && choice.votes) {
          status = Math.floor((choice.votes / allVotes) * 100);
        }

        const radioComponent = (
          <Radio
            checked={selectedVote === choice.url}
            onChange={() => setSelectedVote(choice.url)}
          >
            Select: {choice.choice}
          </Radio>
        );
        return [
          ...acc,
          {
            choice: choice.choice,
            votes: choice.votes,
            percentage: <VoteStatus status={status} />,
            select: radioComponent,
          },
        ];
      }, []),
    [selectedVote, question]
  );

  return (
    <>
      <Table data={data}>
        <Table.Column prop="choice" label="Choice" />
        <Table.Column prop="votes" label="Votes" />
        <Table.Column prop="percentage" label="Percentage" />
        <Table.Column prop="select" label="Select" />
      </Table>
      <Spacer y={2} />
      <Grid.Container gap={2} justify="flex-end">
        <Grid xs={24} md={8}>
          <Button
            loading={saveStatus === "loading"}
            disabled={!selectedVote}
            onClick={onSave}
          >
            Save Votes
          </Button>
        </Grid>
      </Grid.Container>
    </>
  );
};

export default QuestionDetails;
