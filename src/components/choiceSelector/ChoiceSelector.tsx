import React, { useState, FC, useEffect } from "react";
import { Tag, Input, Spacer, Text } from "@zeit-ui/react";
import { X } from "@zeit-ui/react-icons";

import styles from "./ChoiceSelector.module.scss";

interface ChoiceSelectorI {
  onChange: (choices: string[]) => void;
  error: boolean;
}

const ChoiceSelector: FC<ChoiceSelectorI> = ({ onChange, error }) => {
  const [choices, setChoices] = useState<string[]>([]);

  const [inputVal, setInputVal] = useState("");

  useEffect(() => {
    onChange(choices);
  }, [choices]);

  const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const text = e.target.value;
    if (text.includes(",")) {
      const arr = text.split(",");
      if (arr[0].length > 0) {
        setInputVal("");
        setChoices((prevChoices) => [...prevChoices, arr[0]]);
      }
    } else {
      setInputVal(e.target.value);
    }
  };

  const onKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const tmpTarget = event.target as HTMLInputElement;
    if (event.key === "Backspace" && tmpTarget.value === "") {
      const tmpChipsData = [...choices];
      tmpChipsData.pop();
      setChoices(tmpChipsData);
    }
  };

  const removeTag = (choice: string) => {
    setChoices((prevChoices) =>
      prevChoices.filter((prevChoice) => prevChoice !== choice)
    );
  };

  return (
    <div>
      <Text h5>Enter the choices</Text>
      <Text small type="secondary">
        Enter comma-separated values. Use "Backspace" to remove them
      </Text>
      <Spacer y={0.5} />
      <div className={styles.tagContainer}>
        {choices.map((choice) => (
          <Tag key={choice} className={styles.tag} type="lite">
            <span>{choice}</span>{" "}
            <span onClick={() => removeTag(choice)}>
              <X size={12} />
            </span>
          </Tag>
        ))}
      </div>
      <Spacer y={0.5} />
      <Input
        width="100%"
        onKeyDown={onKeyPress}
        value={inputVal}
        onChange={onInputChange}
      />
    </div>
  );
};

export default ChoiceSelector;
