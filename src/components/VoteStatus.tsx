import React, { FC } from "react";
import { Capacity, useTheme } from "@zeit-ui/react";

const VoteStatus: FC<{ status: number }> = ({ status }) => {
  const theme = useTheme();
  return (
    <span>
      <span>{status}%</span>
      <span>
        <Capacity color={theme.palette.success} value={status} />
      </span>
    </span>
  );
};

export default VoteStatus;
