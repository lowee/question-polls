import React, { FC, useMemo } from "react";
import { Question } from "../../types/question.types";
import { Card, Divider, Tag, Text } from "@zeit-ui/react";
import styles from "./QuestionCard.module.scss";

interface QuestionCardProps {
  question: Question;
}

const QuestionCard: FC<QuestionCardProps> = ({ question }) => {
  // catching browser specific errors of date parsing
  const date = useMemo(() => {
    try {
      return new Date(question.published_at).toLocaleDateString();
    } catch (e) {
      console.error(e);
      return "invalid Date";
    }
  }, [question.published_at]);

  return (
    <Card shadow className={styles.card}>
      <div>{question.question}</div>
      <Text small type="secondary">
        Published at: {date}
      </Text>
      <Divider />
      <Tag>Choices: {question.choices.length}</Tag>
    </Card>
  );
};

export default QuestionCard;
