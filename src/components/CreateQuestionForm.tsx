import React, { useCallback, FC } from "react";
import { Card, Input, Button, Divider, Text } from "@zeit-ui/react";
import { Formik, Form, Field, FieldProps } from "formik";
import ChoiceSelector from "./choiceSelector/ChoiceSelector";

export interface CreateQuestionFormValues {
  question: string;
  choices: string[];
}

interface CreateQuestionFormProps {
  onSubmitForm: (values: CreateQuestionFormValues) => void;
  loading: boolean;
}

const CreateQuestionForm: FC<CreateQuestionFormProps> = (props) => {
  const renderSelector = ({ form }) => {
    const changing = (val) => {
      form.setFieldValue("choices", val);
    };
    return <ChoiceSelector error={!!form.errors.choices} onChange={changing} />;
  };

  const renderTitleInput = useCallback(
    ({ form, field }: FieldProps) => (
      <Input
        {...field}
        status={form.errors.question ? "error" : undefined}
        label="Question title"
        width="100%"
      />
    ),
    []
  );

  const validatePresence = (val) => {
    let errorMessage;
    if (val.length < 1) {
      errorMessage = "This field needs a value";
    }
    return errorMessage;
  };

  return (
    <Card>
      <Formik
        initialValues={{ choices: [], question: "" }}
        onSubmit={props.onSubmitForm}
      >
        {({ errors, touched }) => (
          <Form>
            <Field
              validate={validatePresence}
              name="question"
              component={renderTitleInput}
            />
            {errors.question && touched.question ? (
              <Text type="error" small>
                {errors.question}
              </Text>
            ) : null}
            <Divider />
            <Field
              validate={validatePresence}
              name="choices"
              component={renderSelector}
            />
            {errors.choices && touched.choices ? (
              <Text type="error" small>
                {errors.question}
              </Text>
            ) : null}
            <Divider />
            <Button type="success" htmlType="submit">
              Submit
            </Button>
          </Form>
        )}
      </Formik>
    </Card>
  );
};

export default CreateQuestionForm;
