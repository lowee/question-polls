export const mapDataToQuestionId = (data) =>
  data.reduce(
    (acc, val) => ({
      ...acc,
      [val.url.split("/questions/")[1]]: val,
    }),
    {}
  );
