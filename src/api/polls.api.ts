import axios from "axios";
import { mapDataToQuestionId } from "./helpers";
import { CreateQuestionFormValues } from "../components/CreateQuestionForm";

const instance = axios.create({
  baseURL: "https://polls.apiblueprint.org/",
  headers: { "Content-Type": "application/json" },
});

export const getQuestions = async () => {
  const { data } = await instance.get("/questions");
  return mapDataToQuestionId(data);
};

export const voteOnChoice = async (questionId: string, choiceId: string) => {
  const { data } = await instance.post(
    `/questions/${questionId}/choices/${choiceId}`
  );
  return data;
};

export const createQuestion = async (body: CreateQuestionFormValues) => {
  const { data } = await instance.post("/questions", body);
  return data;
};

export default instance;
