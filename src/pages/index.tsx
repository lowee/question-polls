import React from "react";
import { Spinner } from "@zeit-ui/react";
import Layout from "../components/Layout";
import QuestionList from "../components/questionList/QuestionList";
import styles from "./index.module.scss";
import { useQuestionsContext } from "../contexts/question.context";

export default function QuestionsPage() {
  const { status, data } = useQuestionsContext();

  return (
    <Layout>
      {status === "loading" && (
        <div className={styles.loadingWrapper}>
          <Spinner size="large" />
        </div>
      )}
      {status === "success" && <QuestionList questions={data} />}
    </Layout>
  );
}
