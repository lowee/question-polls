import React, { useState } from "react";
import Layout from "../../components/Layout";
import NextLink from "next/link";
import Router from "next/router";
import { useQuestionsContext } from "../../contexts/question.context";
import { Text, Spacer, Link, useToasts } from "@zeit-ui/react";
import CreateQuestionForm, {
  CreateQuestionFormValues,
} from "../../components/CreateQuestionForm";
import { createQuestion } from "../../api/polls.api";

function AddQuestion() {
  const { addQuestion } = useQuestionsContext();

  const [saveLoading, setSaveLoading] = useState(false);
  const [, setToast] = useToasts();

  const onSubmitForm = async (values: CreateQuestionFormValues) => {
    setSaveLoading(true);
    try {
      const newQuestion = await createQuestion(values);
      const id = newQuestion.url.split("/questions/")[1];
      addQuestion(id, newQuestion);
      Router.push(`/questions/${id}`);
    } catch (e) {
      setToast({
        text: "Something went wrong. The question could not be created",
        type: "error",
      });
    } finally {
      setSaveLoading(false);
    }
  };

  return (
    <Layout>
      <Text h3>Create a new Question:</Text>
      <NextLink href="/">
        <Link color>Back to overview</Link>
      </NextLink>
      <Spacer y={2} />
      <CreateQuestionForm onSubmitForm={onSubmitForm} loading={saveLoading} />
    </Layout>
  );
}

export default AddQuestion;
