import React, { useState } from "react";
import Layout from "../../components/Layout";
import NextLink from "next/link";
import { Text, Spacer, Link } from "@zeit-ui/react";
import { useRouter } from "next/router";
import QuestionDetails from "../../components/QuestionDetails";
import { Spinner } from "@zeit-ui/react";
import { useQuestionsContext } from "../../contexts/question.context";
import { voteOnChoice } from "../../api/polls.api";
import styles from "../index.module.scss";

export type SaveStates = "success" | "loading" | "error";

function QuestionDetailPage() {
  const router = useRouter();
  const { questionId } = router.query;
  const { status, data, updateQuestion } = useQuestionsContext();
  const [saveStatus, setSaveStatus] = useState<SaveStates>("success");

  const saveVotes = async (choiceId) => {
    setSaveStatus("loading");
    try {
      const updatedChoice = await voteOnChoice(questionId as string, choiceId);
      updateQuestion(questionId as string, updatedChoice);
      setSaveStatus("success");
    } catch (e) {
      setSaveStatus("error");
    }
  };

  return (
    <Layout>
      <Text h3>
        Question:{" "}
        {status === "success" && (
          <span>{data[questionId as string].question}</span>
        )}
      </Text>
      <Spacer y={2} />
      <NextLink href="/">
        <Link color>Back to overview</Link>
      </NextLink>
      <Spacer y={1} />
      {status === "loading" && (
        <div className={styles.loadingWrapper}>
          <Spinner size="large" />
        </div>
      )}
      {status === "success" && (
        <QuestionDetails
          saveStatus={saveStatus}
          saveVotes={saveVotes}
          question={data[questionId as string]}
        />
      )}
    </Layout>
  );
}

export default QuestionDetailPage;
