import { AppProps } from "next/app";
import { CSSBaseline, ZEITUIProvider } from "@zeit-ui/react";
import { QuestionsProvider } from "../contexts/question.context";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <QuestionsProvider>
      <ZEITUIProvider>
        <CSSBaseline />
        <Component {...pageProps} />
      </ZEITUIProvider>
    </QuestionsProvider>
  );
}

export default MyApp;
