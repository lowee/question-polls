import { useQuery } from "react-query";
import { getQuestions } from "../api/polls.api";
import { QuestionsData } from "../types/question.types";

export function useQuestions() {
  return useQuery<QuestionsData, "questions">("questions", getQuestions);
}
