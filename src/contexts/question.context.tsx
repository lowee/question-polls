import { useContext, createContext, useState, useEffect } from "react";
import { QuestionsData, Choice, Question } from "../types/question.types";
import { useQuestions } from "../hooks/useQuestions";
import { QueryResult, QueryLoadingResult } from "react-query";
import { stringify } from "querystring";

interface CustomQuestionContext {
  updateQuestion: (id: string, choice: Choice) => void;
  addQuestion: (id: string, choice: Choice) => void;
}

type QuestionContextI = QueryResult<QuestionsData> & CustomQuestionContext;

export const QuestionsContext = createContext<QuestionContextI>(null!);

const useQuestionsContext = () => {
  const context = useContext(QuestionsContext);
  if (!context) {
    throw new Error(
      "useQuestionContext must be used within a QuestionsProvider"
    );
  }
  return context;
};

const QuestionsProvider = (props) => {
  const query = useQuestions();
  const [data, setData] = useState(query.data);

  useEffect(() => {
    setData(query.data);
  }, [query.data]);

  const updateQuestion = (id: string, choice: Choice) => {
    setData((prevData) => {
      const changedQuestion = { ...prevData[id] };
      const changedChoiceIndex = changedQuestion.choices.findIndex(
        (el) => el.url === choice.url
      );
      if (changedChoiceIndex !== -1) {
        changedQuestion.choices[changedChoiceIndex] = choice;
      }
      return {
        ...prevData,
        [id]: changedQuestion,
      };
    });
  };

  const addQuestion = (id: string, question: Question) => {
    setData((prevData) => ({ [id]: question, ...prevData }));
  };

  const value = {
    ...query,
    data: data || query.data,
    updateQuestion,
    addQuestion,
  };

  return <QuestionsContext.Provider value={value} {...props} />;
};

export { useQuestionsContext, QuestionsProvider };
