## Trial Application 🧪

Hello, this is a trial app using the api from https://pollsapi.docs.apiary.io/.

## Getting Started 🚀

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Or visit the live deployment here: https://question-polls.now.sh/

### Tech stack 📚

#### Language

To have proper typings, I have used typescript in all my files.

#### Framework

As a framework on top of react I've chosen [Next.js](https://nextjs.org/). For improved page speed and other advantages.

#### Ui-library

As a ui-library I chose `@zeit-ui/react` together with `@zeit-ui/react-icons`
https://react.zeit-ui.co/

#### Styles

Most of the styles are coming from the ui-library, if not I've used the css-modules pattern together with sass.

#### Global state management

For all global states I used the context-api wrapped in the `_app.tsx` component.

#### Data fetching

For data fetching I have chosen [axios](https://github.com/axios/axios) in combination with [react-query](https://github.com/tannerlinsley/react-query)

### Comments 💬

- For all my commit messages I used the [gitmoji](https://gitmoji.carloscuesta.me/) pattern. I really like the structure of writing commit messages with it.

- The api did not provide a proper `id` field. I've extracted the id from the url value. This is not really ideal but I couldn't found a better solution to have a really unique id.

- There is definitely a lot to improve when it comes to error handling. For the voting system I added toast message as a feedback.

- I also built this choice selector component. Which should work properly, but since I just built this on the fly, there are for sure some edge cases I couldn't covered.

- I also would normally store all the constants, urls etc. in `env` variables. But for this small application, I thought it's okay to store them hard-coded.

#### Feel free to contact me, if you have any questions about the code or how to make it run
